﻿using UnityEngine;
using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Create : MonoBehaviour {



    public string servername, databasename, username, password;

	Button Create_Button , Back_Button;




    string constr;

    MySqlConnection con = null;
    // command object
    MySqlCommand cmd = null;
    // reader object
    MySqlDataReader rdr = null;

    public GameObject textPrefab, buttonPrefab;


	public string in_uname = "";
	public string in_pword;
    public int idtoupdate;


    void Awake()
    {

        DontDestroyOnLoad(this);
		DontDestroyOnLoad(Back_Button);

        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(this.gameObject);
        }


        try
        {
            // setup the connection element

            constr = String.Format("Server={0};Database={1};User={2};Password={3};Pooling=false",
                servername, databasename, username, password);
            Debug.Log(constr);
            con = new MySqlConnection(constr);

            // lets see if we can open the connection
            con.Open();
            Debug.Log("Connection State: " + con.State);
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }

    }

    void OnApplicationQuit()
    {
        Debug.Log("killing con");
        if (con != null)
        {
            if (con.State.ToString() != "Closed")
                con.Close();
            con.Dispose();
        }
    }

    public void insertData(string in_uname, string in_pword)
    {
        try
        {
            string query = "";
            query = "INSERT INTO Users (Username, Password) VALUES (?Username, ?Password)";
            if (con.State.ToString() != "Open")
                con.Open();

            using (con)
            {
                using (cmd = new MySqlCommand(query, con))
                {
                    MySqlParameter oParam1 = cmd.Parameters.Add("?Username", MySqlDbType.VarChar);
                    oParam1.Value = in_uname;
                    MySqlParameter oParam2 = cmd.Parameters.Add("?Password", MySqlDbType.VarChar);
                    oParam2.Value = in_pword;
                    cmd.ExecuteNonQuery();
					SceneManager.LoadScene("Start_Up");
                }
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }





	// Use this for initialization
	void Start () {
        Create_Button = GameObject.Find("Create_Button").GetComponent<Button>();
        Create_Button.onClick.AddListener(Create_Button_Pressed);
		Back_Button = GameObject.Find ("Back_Button").GetComponent<Button> ();
		//Back_Button.onClick.AddListener(Back_Button_Pressed);
	}		

    void Create_Button_Pressed()
    {		
        in_uname = GameObject.Find("Username_InputField").GetComponent<InputField>().text;
        in_pword = GameObject.Find("Password_InputField").GetComponent<InputField>().text;

		if (in_uname != "" && in_pword != "") {
			insertData (in_uname, in_pword);
		}
		else {
			Debug.Log("Error");
		}
        //Debug.Log(in_uname + in_pword);
       
    }
	// Update is called once per frame
	void Update () {
		//Back_Button.onClick.AddListener(Back_Button_Pressed);
	}

	public void Back_Button_Pressed(string Level_Name){
		SceneManager.LoadScene(Level_Name);
	}
}
