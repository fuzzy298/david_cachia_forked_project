﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Input : MonoBehaviour {
	Button Log_In , New_Profile;


	void Start () {
		Log_In = GameObject.Find ("Log_In").GetComponent<Button> ();
		New_Profile = GameObject.Find ("New_Profile").GetComponent<Button> ();

		Log_In.onClick.AddListener (Log_In_Pressed);
		New_Profile.onClick.AddListener (New_Profile_Pressed);
	}

	void Log_In_Pressed()
	{
		SceneManager.LoadScene ("Log_In");
	}

	void New_Profile_Pressed()
	{
		SceneManager.LoadScene ("Create");
	}
}

