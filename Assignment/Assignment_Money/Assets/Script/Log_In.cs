﻿using UnityEngine;
using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Log_in : MonoBehaviour {

	public string servername,databasename,username,password;


	Button Log_In_Button , Back_Button;
	

	
	string constr; 

	MySqlConnection con = null;
	// command object
	MySqlCommand cmd = null;
	// reader object
	MySqlDataReader rdr = null;

	public GameObject textPrefab,buttonPrefab;

	
	public static string uname,pword;
	public int idtoupdate;


	void Awake()
	{

		DontDestroyOnLoad (this);

		if (FindObjectsOfType (GetType ()).Length > 1) {
			Destroy (this.gameObject);
		}


		try
		{
			// setup the connection element

			constr = String.Format("Server={0};Database={1};User={2};Password={3};Pooling=false",
				servername,databasename,username,password);
			Debug.Log(constr);
			con = new MySqlConnection(constr);

			// lets see if we can open the connection
			con.Open();
			Debug.Log("Connection State: " + con.State);
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}

	}

	void OnApplicationQuit()
	{
		Debug.Log("killing con");
		if (con != null)
		{
			if (con.State.ToString() != "Closed")
				con.Close();
			con.Dispose();
		}
	}


	void SearchDataByName(string name,string password)
	{
		try
		{
			string query = "";
			query = String.Format("SELECT * FROM Users WHERE Username ='{0}' AND password = '{1}'",name,password);
			Debug.Log(query);
			if (con.State.ToString() != "Open")
				con.Open();

			using (con)
			{
				using (cmd = new MySqlCommand(query, con))
				{
					rdr = cmd.ExecuteReader();

					if (rdr.HasRows) 
					{
                        SceneManager.LoadScene("Main");
						Debug.Log("Correct");
					}
					rdr.Dispose();
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
			Debug.Log("Incorrect");
		}
	}
	// Use this for initialization
	
	void Start () {
		Log_In_Button = GameObject.Find("Log_In_Button").GetComponent<Button>();
		Back_Button = GameObject.Find("Back_Button").GetComponent<Button>();
		Log_In_Button.onClick.AddListener(Log_In_Button_Pressed);
		Back_Button.onClick.AddListener(Back_Button_Pressed);
	}
	// Update is called once per frame
	void Update () {

	}
	
	void Log_In_Button_Pressed(){
		uname = GameObject.Find("username").GetComponent<InputField>().text;
		pword = GameObject.Find("password").GetComponent<InputField>().text;
		Debug.Log(uname + pword);
		SearchDataByName (uname,pword);
	}

	void Back_Button_Pressed(){
		SceneManager.LoadScene("Screen_One");
	}
}
