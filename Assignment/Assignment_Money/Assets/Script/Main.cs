﻿using UnityEngine;
using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Main : MonoBehaviour {

	public string servername,databasename,username,password;


	string uname1 = Log_in.uname;
	int id_number, Income,Expences,Current, Budget, budgetnum;
	Log_in log;
	// Use this for initialization

	string constr;
	Text TotalMoney_Number,TotalIncome_Number,TotalLoss_Number,TotalBudget_Number;
	InputField Update_Income_InputField , Update_Expences_InputField, Bonus_OneTime, Expences_OneTime;
	Button Income_Update , Expences_Update,Bonus_Button,Expence_Button,Buget_Button,Ref_Button;
	Dropdown Intensive_Dropdown;

	MySqlConnection con = null;
	// command object
	MySqlCommand cmd = null;
	// reader object
	MySqlDataReader rdr = null;

	public GameObject textPrefab,buttonPrefab;

	public int idtoupdate;
	public Text txt;
	string budgetwork, budgetmain,Update_Income,Update_Expences, Income_add,Expenses_add, buget_number;
	int bdgt, counter1,counter2,day_num;
	float buget_counter;

	void Awake()
	{

		DontDestroyOnLoad (this);

		if (FindObjectsOfType (GetType ()).Length > 1) {
			Destroy (this.gameObject);
		}


		try
		{
			// setup the connection element

			constr = String.Format("Server={0};Database={1};User={2};Password={3};Pooling=false",
				servername,databasename,username,password);
			Debug.Log(constr);
			con = new MySqlConnection(constr);

			// lets see if we can open the connection
			con.Open();
			Debug.Log("Connection State: " + con.State);
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}

	}

	void OnApplicationQuit()
	{
		Debug.Log("killing con");
		if (con != null)
		{
			if (con.State.ToString() != "Closed")
				con.Close();
			con.Dispose();
		}
	}

	void SearchDataByName(string name)
	{
		try
		{
			string query = "";
			query = String.Format("SELECT Id FROM Users WHERE Username ='{0}'",name);
			if (con.State.ToString() != "Open")
				con.Open();

			using (con)
			{
				using (cmd = new MySqlCommand(query, con))
				{
					rdr = cmd.ExecuteReader();
					if (rdr.HasRows) 
					{ 
						while (rdr.Read())
						{
							id_number = int.Parse(rdr["Id"].ToString());
							Debug.Log(id_number);

						}
					}
					rdr.Dispose();
					SearchDataByName2(id_number);
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}
	}





	void SearchDataByName2(int name)
	{
		try
		{
			string query = "";
			query = String.Format("SELECT * FROM Money WHERE Id = '{0}'",name);
			if (con.State.ToString() != "Open")
				con.Open();

			using (con)
			{
				using (cmd = new MySqlCommand(query, con))
				{
					rdr = cmd.ExecuteReader();
					if (rdr.HasRows) { 
						while (rdr.Read())
						{
							
							Income = int.Parse(rdr["Income"].ToString());
							Expences = int.Parse(rdr["Expences"].ToString());
							Current = int.Parse(rdr["Current"].ToString());
							Budget = int.Parse(rdr["Budget"].ToString());
							//budgetnum = int.Parse(rdr["Budget_am"].ToString());

							//bdgt = int.TryParse(Budget.ToString);

							TotalMoney_Number.text = Current + "";
							TotalIncome_Number.text = Income + "";
							TotalLoss_Number.text = Expences + "";
							//TotalBudget_Number.text = budgetnum + "";
							//Debug.Log(budgetnum);


							budgetmain = Income + "";

							budgetwork = Budget + "";
							Debug.Log(budgetwork);
							if (Budget == 0 ){
								Debug.Log("10");
								int total = (10 * int.Parse(budgetmain))/100;
								//total = Current - total;
								TotalBudget_Number.text = total + "";
								BugetAM(id_number,total);
							}
							if (Budget == 1 ){
								Debug.Log("25");
								int total = (25 * int.Parse(budgetmain))/100;
								//total = Current - total;
								TotalBudget_Number.text = total + "";
								BugetAM(id_number,total);
							}
							if (Budget == 2 ){
								Debug.Log(budgetmain);
								Debug.Log(Income);
								int total = (50 * int.Parse(budgetmain))/100;
								//total = Current - total;
								TotalBudget_Number.text = total + "";
								BugetAM(id_number,total);
							}
							if (Budget == 3 ){
								Debug.Log("75");
								int total = (75 * int.Parse(budgetmain))/100;
								//total = Current - total;
								TotalBudget_Number.text = total + "";
								BugetAM(id_number,total);
							}
							if (Budget == 4 ){
								Debug.Log("90");
								int total = (90 * int.Parse(budgetmain))/100;
								//total = Current - total;
								TotalBudget_Number.text = total + "";
								BugetAM(id_number,total);
							}

						}
					}
					rdr.Dispose();
					//Money();
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}
	}

	public void updateRecord(string Income, int Id)
	{
		try
		{
			string query = "";
			query = "UPDATE Money SET Income = ?Income WHERE Id  = ?Id ";
			if (con.State.ToString() != "Open")
				con.Open();

			using (con)
			{
				using (cmd = new MySqlCommand(query, con))
				{
					Debug.Log("In");
					Debug.Log(Income);
					MySqlParameter oParam1 = cmd.Parameters.Add("?Income", MySqlDbType.VarChar);
					oParam1.Value = Income;
					MySqlParameter oParam2 = cmd.Parameters.Add("?Id", MySqlDbType.Int32);
					oParam2.Value = Id;
					cmd.ExecuteNonQuery();
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}

	}


	public void updateRecord1(string Expences, int Id)
	{
		try
		{
			string query = "";
			query = "UPDATE Money SET Expences = ?Expences WHERE Id  = ?Id ";
			if (con.State.ToString() != "Open")
				con.Open();

			using (con)
			{
				using (cmd = new MySqlCommand(query, con))
				{
					Debug.Log("In");
					Debug.Log(Expences);
					MySqlParameter oParam1 = cmd.Parameters.Add("?Expences", MySqlDbType.VarChar);
					oParam1.Value = Expences;
					MySqlParameter oParam2 = cmd.Parameters.Add("?Id", MySqlDbType.Int32);
					oParam2.Value = Id;
					cmd.ExecuteNonQuery();
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}

	}




	void SearchDataByNameAdd1(int Id)
	{
		try
		{
			string query = "";
			query = String.Format("SELECT * FROM Money WHERE Id = '{0}'",Id);
			if (con.State.ToString() != "Open")
				con.Open();

			using (con)
			{
				using (cmd = new MySqlCommand(query, con))
				{
					rdr = cmd.ExecuteReader();
					if (rdr.HasRows) { 
						while (rdr.Read())
						{
							Income = int.Parse(rdr["Current"].ToString());
							Debug.Log(Income);
							Income = Income + int.Parse(Income_add);
							Debug.Log(Income);


						}
					}
					rdr.Dispose();
					updateRecordAdd1(id_number,Income);
					//Money();
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}
	}

	public void updateRecordAdd1(int Id, int Income)
	{
		try
		{
			string query = "";
			query = "UPDATE Money SET Current = ?Current WHERE id=?id";
			if (con.State.ToString() != "Open")
				con.Open();

			using (con)
			{
				using (cmd = new MySqlCommand(query, con))
				{
					MySqlParameter oParam1 = cmd.Parameters.Add("?Current", MySqlDbType.Int32);
					oParam1.Value = Income;
					MySqlParameter oParam2 = cmd.Parameters.Add("?Id", MySqlDbType.Int32);
					oParam2.Value = Id;
					cmd.ExecuteNonQuery();
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}

	}



	void SearchDataByNameAdd2(int Id)
	{
		try
		{
			string query = "";
			query = String.Format("SELECT * FROM Money WHERE Id = '{0}'",Id);
			if (con.State.ToString() != "Open")
				con.Open();

			using (con)
			{
				using (cmd = new MySqlCommand(query, con))
				{
					rdr = cmd.ExecuteReader();
					if (rdr.HasRows) { 
						while (rdr.Read())
						{
							Income = int.Parse(rdr["Current"].ToString());
							Debug.Log(Income);
							Income = Income - int.Parse(Expenses_add);
							Debug.Log(Income);

							buget_counter = int.Parse(TotalBudget_Number.text);
							//Debug.Log(buget_counter);

							buget_counter = buget_counter - int.Parse(Expenses_add);
							Debug.Log(buget_counter);


						}
					}
					rdr.Dispose();
					updateRecordAdd2(id_number,Income,buget_counter);
					//Money();
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}
	}



	public void updateRecordAdd2(int Id, int Income, float Budget_am)
	{
		try
		{
			string query = "";
			query = "UPDATE Money SET Current = ?Current , Budget_am = ?Budget_am WHERE id=?id";
			if (con.State.ToString() != "Open")
				con.Open();

			using (con)
			{
				using (cmd = new MySqlCommand(query, con))
				{
					MySqlParameter oParam1 = cmd.Parameters.Add("?Current", MySqlDbType.Int32);
					oParam1.Value = Income;
					MySqlParameter oParam2 = cmd.Parameters.Add("?Id", MySqlDbType.Int32);
					oParam2.Value = Id;
					MySqlParameter oParam3 = cmd.Parameters.Add("?Budget_am", MySqlDbType.Float);
					oParam3.Value = Budget_am;
					cmd.ExecuteNonQuery();
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}

	}


	public void budgetupdate(int Id, String budget)
	{
		try
		{
			string query = "";
			query = "UPDATE Money SET Budget = ?budget WHERE Id=?Id";
			if (con.State.ToString() != "Open")
				con.Open();

			using (con)
			{
				using (cmd = new MySqlCommand(query, con))
				{
					MySqlParameter oParam1 = cmd.Parameters.Add("?Id", MySqlDbType.Int32);
					oParam1.Value = Id;
					MySqlParameter oParam2 = cmd.Parameters.Add("?budget", MySqlDbType.Int32);
					oParam2.Value = budget;

					cmd.ExecuteNonQuery();
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}

	}


	public void BugetAM(int id, int name)
	{
		try
		{
			string query = "";
			query = "UPDATE Money SET Buget_am = ?name WHERE id=?id";
			if (con.State.ToString() != "Open")
				con.Open();

			using (con)
			{
				using (cmd = new MySqlCommand(query, con))
				{
					MySqlParameter oParam1 = cmd.Parameters.Add("?name", MySqlDbType.VarChar);
					oParam1.Value = name;
					MySqlParameter oParam2 = cmd.Parameters.Add("?id", MySqlDbType.Int32);
					oParam2.Value = id;

				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}

	}


	public void monthup(int id, int incnumber)
	{
		try
		{
			string query = "";
			query = "UPDATE Money SET incnumber = ?incnumber WHERE id=?id";
			if (con.State.ToString() != "Open")
				con.Open();

			using (con)
			{
				using (cmd = new MySqlCommand(query, con))
				{
					MySqlParameter oParam1 = cmd.Parameters.Add("?incnumber", MySqlDbType.Int32);
					oParam1.Value = incnumber;
					MySqlParameter oParam2 = cmd.Parameters.Add("?id", MySqlDbType.Int32);
					oParam2.Value = id;
					cmd.ExecuteNonQuery();
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}

	}




	void Start () {			
		Update_Expences = GameObject.Find ("Update_Expences_InputField").GetComponent<InputField>().text;
		//Expences_Update = GameObject.Find ("Update_Expences_InputField").GetComponent<InputField>().text;

		Income_Update = GameObject.Find("Income_Update").GetComponent<Button>();
		Expences_Update = GameObject.Find("Expences_Update").GetComponent<Button>();
		Bonus_Button = GameObject.Find("Bonus_Button").GetComponent<Button>();
		Expence_Button = GameObject.Find("Expence_Button").GetComponent<Button>();
		Buget_Button = GameObject.Find("Buget_Button").GetComponent<Button>();
		Ref_Button = GameObject.Find("Ref_Button").GetComponent<Button>();

		Income_Update.onClick.AddListener(Update1);	
		Expences_Update.onClick.AddListener(Update2);
		Bonus_Button.onClick.AddListener(add1);
		Expence_Button.onClick.AddListener(add2);
		Buget_Button.onClick.AddListener(alter);
		Ref_Button.onClick.AddListener(refresh);

		TotalMoney_Number = GameObject.Find ("TotalMoney_Number").GetComponent<Text> ();
		TotalIncome_Number = GameObject.Find ("TotalIncome_Number").GetComponent<Text> ();
		TotalLoss_Number = GameObject.Find ("TotalLoss_Number").GetComponent<Text> ();
		TotalBudget_Number = GameObject.Find ("TotalBudget_Number").GetComponent<Text> ();
		SearchDataByName (uname1);

		day_num = DateTime.Now.Day;
		int incnumber = int.Parse(TotalIncome_Number.text);
		int totalnumber = int.Parse(TotalMoney_Number.text);
		totalnumber = totalnumber + incnumber;
		Debug.Log (totalnumber);
		if (counter1 == 1) {
			if (day_num == 1) {
				monthup (id_number, totalnumber);
				counter1 = 2;
			}
		}
		if (day_num == 2) {
			counter1 = 1;
		}
	}
	

	void Update () {

	}

	void Update1(){
		Update_Income = GameObject.Find("Update_Income_InputField").GetComponent<InputField>().text;
		Debug.Log (Update_Income);
		if (Update_Income != "")
			{
			Debug.Log (Update_Income);
			try{
				int.Parse(Update_Income);
				Debug.Log (Update_Income);
				updateRecord (Update_Income, id_number);
			}catch (Exception e) {
				Debug.Log ("Error");
			}
		}
		else {
			Debug.Log("Error");
		}

	}


	void Update2(){
		Update_Expences = GameObject.Find ("Update_Expences_InputField").GetComponent<InputField>().text;
		//Intensive_Dropdown = GameObject.Find("Intensive_Dropdown").GetComponent<Dropdown>();
		Debug.Log (Update_Expences);
		if (Update_Expences != "")
		{
			Debug.Log (Update_Expences);
			try{
				int.Parse(Update_Expences);
				Debug.Log (Update_Expences);
				updateRecord1 (Update_Expences, id_number);
			}catch (Exception e) {
				Debug.Log ("Error");
			}
		}
		else {
			Debug.Log("Error");
		}

	}

	void add1(){
		Income_add = GameObject.Find("Bonus_OneTime").GetComponent<InputField>().text;
		Debug.Log (Income_add);

		if (Income_add != "")
		{
			Debug.Log (Income_add);
			try{
				int.Parse(Income_add);
				Debug.Log (Income_add);
				SearchDataByNameAdd1(id_number);

			}catch (Exception e) {
				Debug.Log ("Error");
			}
		}
		else {
			Debug.Log("Error");
		}

	}

	void add2(){
		Expenses_add = GameObject.Find("Expences_OneTime").GetComponent<InputField>().text;
		Debug.Log (Expenses_add);

		if (Expenses_add != "")
		{
			Debug.Log (Expenses_add);
			try{
				int.Parse(Expenses_add);
				Debug.Log (Expenses_add);
				SearchDataByNameAdd2(id_number);

			}catch (Exception e) {
				Debug.Log ("Error");
			}
		}
		else {
			Debug.Log("Error");
		}

	}

	void alter(){
		Intensive_Dropdown = GameObject.Find("Intensive_Dropdown").GetComponent<Dropdown>();
		buget_number = Intensive_Dropdown.value.ToString();
		//Intensive_Dropdown = (Intensive_Dropdown).ToString;
		Debug.Log (buget_number);
		budgetupdate(id_number , buget_number);
	}

	void refresh(){
		SearchDataByName (uname1);
	}

}
