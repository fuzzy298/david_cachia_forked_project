﻿using UnityEngine;
using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Start_up : MonoBehaviour {

	public string servername, databasename, username, password;

	InputField Current_Money_InputField ,Monthly_income, Constant_exp;
	Dropdown weaklybudget;
	Button Start_Button;

	string constr;

	MySqlConnection con = null;
	// command object
	MySqlCommand cmd = null;
	// reader object
	MySqlDataReader rdr = null;

	public GameObject textPrefab, buttonPrefab;
	//public Create create;
	//string username1 = "";
	string Income, Expences, Current,Budgets;
	int total,Budget;

	void Awake()
	{

		DontDestroyOnLoad(this);

		if (FindObjectsOfType(GetType()).Length > 1)
		{
			Destroy(this.gameObject);
		}


		try
		{
			// setup the connection element

			constr = String.Format("Server={0};Database={1};User={2};Password={3};Pooling=false",
				servername, databasename, username, password);
			Debug.Log(constr);
			con = new MySqlConnection(constr);

			// lets see if we can open the connection
			con.Open();
			Debug.Log("Connection State: " + con.State);
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}

	}

	void OnApplicationQuit()
	{
		Debug.Log("killing con");
		if (con != null)
		{
			if (con.State.ToString() != "Closed")
				con.Close();
			con.Dispose();
		}
	}

	public void insertData(string Income, string Expences, string Current, string Budgets, int total)
	{
		try
		{
			string query = "";
			query = "INSERT INTO Money (Income, Expences, Current,Budget,Budget_am) VALUES (?Income, ?Expences, ?Current, ?Budgets, ?total)";
			if (con.State.ToString() != "Open")
				con.Open();

			using (con)
			{
				using (cmd = new MySqlCommand(query, con))
				{
					MySqlParameter oParam1 = cmd.Parameters.Add("?Income", MySqlDbType.VarChar);
					oParam1.Value = Income;
					MySqlParameter oParam2 = cmd.Parameters.Add("?Expences", MySqlDbType.VarChar);
					oParam2.Value = Expences;
					MySqlParameter oParam3 = cmd.Parameters.Add("?Current", MySqlDbType.VarChar);
					oParam3.Value = Current;
					MySqlParameter oParam4 = cmd.Parameters.Add("?Budgets", MySqlDbType.VarChar);
					oParam4.Value = Budget;
					MySqlParameter oParam5 = cmd.Parameters.Add("?total", MySqlDbType.VarChar);
					oParam5.Value = total;
					cmd.ExecuteNonQuery();
					SceneManager.LoadScene("Log_In");
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}
	}

	void Start () {
		weaklybudget = GameObject.Find("weaklybudget").GetComponent<Dropdown>();
		Start_Button = GameObject.Find("Start_Button").GetComponent<Button>();
		Current_Money_InputField = GameObject.Find ("Current_Money_InputField").GetComponent<InputField> ();
		Monthly_income = GameObject.Find ("Monthly_income").GetComponent<InputField> ();
		Constant_exp = GameObject.Find ("Constant_exp").GetComponent<InputField> ();

		Start_Button.onClick.AddListener(Start_Button_Pressed);
	}

	// Update is called once per frame
	void Start_Button_Pressed(){
		//username1 = create.in_uname;

		Income = GameObject.Find("Monthly_income").GetComponent<InputField>().text;
		Expences = GameObject.Find("Constant_exp").GetComponent<InputField>().text;
		Current = GameObject.Find("Current_Money_InputField").GetComponent<InputField>().text;
		Budget = weaklybudget.value;


		String budgetmain = Income + "";

		//int.Parse (Budget);
		if (Budget == 0 ){
			Debug.Log(budgetmain);
			total = (10 * int.Parse(budgetmain))/100;
		}
		if (Budget == 1 ){
			Debug.Log("25");
			total = (25 * int.Parse(budgetmain))/100;
		}
		if (Budget == 2 ){
			Debug.Log("50");
			total = (50 * int.Parse(budgetmain))/100;
		}
		if (Budget == 3 ){
			Debug.Log("75");
			total = (75 * int.Parse(budgetmain))/100;
		}
		if (Budget == 4 ){
			Debug.Log("90");
			total = (90 * int.Parse(budgetmain))/100;
		}

		Budgets = weaklybudget.value.ToString();
		Debug.Log (Income +" " + Expences +" "+ Current +" " + Budgets +" "+ total);
		insertData (Income, Expences, Current,Budgets,total );
		//Debug.Log (username1);
	}
}
