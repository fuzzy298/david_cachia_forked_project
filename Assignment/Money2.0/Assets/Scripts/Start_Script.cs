﻿using UnityEngine;
using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Start_Script : MonoBehaviour {

	public string servername,databasename,username,password;


	string constr; 

	MySqlConnection con = null;
	// command object
	MySqlCommand cmd = null;
	// reader object
	MySqlDataReader rdr = null;

	public GameObject textPrefab,buttonPrefab;

	public int idtoupdate;


	void Awake()
	{

		DontDestroyOnLoad (this);

		if (FindObjectsOfType (GetType ()).Length > 1) {
			Destroy (this.gameObject);
		}


		try
		{
			// setup the connection element

			constr = String.Format("Server={0};Database={1};User={2};Password={3};Pooling=false",
				servername,databasename,username,password);
			Debug.Log(constr);
			con = new MySqlConnection(constr);

			// lets see if we can open the connection
			con.Open();
			Debug.Log("Connection State: " + con.State);
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}

	}

	void OnApplicationQuit()
	{
		Debug.Log("killing con");
		if (con != null)
		{
			if (con.State.ToString() != "Closed")
				con.Close();
			con.Dispose();
		}
	}


	void SearchDataByName(string name)
	{
		try
		{
			string query = "";
			query = String.Format("SELECT * FROM score WHERE name LIKE %{0}%",name);
			if (con.State.ToString() != "Open")
				con.Open();

			using (con)
			{
				using (cmd = new MySqlCommand(query, con))
				{
					rdr = cmd.ExecuteReader();
					GameObject eachRecord = Instantiate(textPrefab, GameObject.Find("Content").transform, false) as GameObject;
					eachRecord.GetComponent<Text>().text = "id" + " " + "name" + " " + "score";
					if (rdr.HasRows) { 
						while (rdr.Read())
						{
							eachRecord = Instantiate(textPrefab, GameObject.Find("Content").transform, false) as GameObject;
							eachRecord.GetComponent<Text>().text = rdr["id"] + " " + rdr["name"] + " " + rdr["score"];
							GameObject updateButton = Instantiate(buttonPrefab,GameObject.Find("Content").transform,false) as GameObject;
							updateButton.GetComponentInChildren<Text>().text = "Update:" + rdr["id"];
							GameObject deleteButton = Instantiate(buttonPrefab,GameObject.Find("Content").transform,false) as GameObject;
							deleteButton.GetComponentInChildren<Text>().text = "Delete:" + rdr["id"];

							int id = int.Parse(rdr["id"].ToString());

							updateButton.GetComponent<Button>().onClick.AddListener(() => {
								updateButtonPressed(id);
							});

							deleteButton.GetComponent<Button>().onClick.AddListener(() => {
								deleteButtonPressed(id);
							});
						}
					}
					rdr.Dispose();
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}
	}


	void GetData()
	{
		try
		{
			string query = "";
			query = "SELECT * FROM score";
			if (con.State.ToString() != "Open")
				con.Open();

			using (con)
			{
				using (cmd = new MySqlCommand(query, con))
				{
					rdr = cmd.ExecuteReader();
					GameObject eachRecord = Instantiate(textPrefab, GameObject.Find("Content").transform, false) as GameObject;
					eachRecord.GetComponent<Text>().text = "id" + " " + "name" + " " + "score";
					if (rdr.HasRows) { 
						while (rdr.Read())
						{
							eachRecord = Instantiate(textPrefab, GameObject.Find("Content").transform, false) as GameObject;
							eachRecord.GetComponent<Text>().text = rdr["id"] + " " + rdr["name"] + " " + rdr["score"];
							GameObject updateButton = Instantiate(buttonPrefab,GameObject.Find("Content").transform,false) as GameObject;
							updateButton.GetComponentInChildren<Text>().text = "Update:" + rdr["id"];
							GameObject deleteButton = Instantiate(buttonPrefab,GameObject.Find("Content").transform,false) as GameObject;
							deleteButton.GetComponentInChildren<Text>().text = "Delete:" + rdr["id"];

							int id = int.Parse(rdr["id"].ToString());

							updateButton.GetComponent<Button>().onClick.AddListener(() => {
								updateButtonPressed(id);
							});

							deleteButton.GetComponent<Button>().onClick.AddListener(() => {
								deleteButtonPressed(id);
							});
						}
					}
					rdr.Dispose();
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}
	}

	public void GetOneRecord(int id)
	{
		try
		{
			string query = "";
			query = "SELECT * FROM score where id = ?id";
			if (con.State.ToString() != "Open")
				con.Open();

			using (con)
			{
				using (cmd = new MySqlCommand(query, con))
				{
					MySqlParameter oParam1 = cmd.Parameters.Add("?id", MySqlDbType.Int32);
					oParam1.Value = id;
					rdr = cmd.ExecuteReader();
					//GameObject eachRecord = Instantiate(textPrefab, GameObject.Find("Content").transform, false) as GameObject;
					//eachRecord.GetComponent<Text>().text = "id" + " " + "name" + " " + "score";
					if (rdr.HasRows) { 
						while (rdr.Read())
						{
							if (SceneManager.GetActiveScene().name == "update")
							{
								GameObject.Find("ScoreField").GetComponent<InputField>().text = rdr["score"].ToString();
								GameObject.Find("NameField").GetComponent<InputField>().text = rdr["name"].ToString();
								GameObject.Find("updateRecordButton").GetComponent<Button>().onClick.AddListener(() => {
									updateRecord(id,GameObject.Find("NameField").GetComponent<InputField>().text,int.Parse(GameObject.Find("ScoreField").GetComponent<InputField>().text));
								});
							}

						}
					}
					rdr.Dispose();
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}
	}

	public void updateRecord(int id, string name, int score)
	{
		try
		{
			string query = "";
			query = "UPDATE score SET name = ?name, score = ?score WHERE id=?id";
			if (con.State.ToString() != "Open")
				con.Open();

			using (con)
			{
				using (cmd = new MySqlCommand(query, con))
				{
					MySqlParameter oParam1 = cmd.Parameters.Add("?name", MySqlDbType.VarChar);
					oParam1.Value = name;
					MySqlParameter oParam2 = cmd.Parameters.Add("?score", MySqlDbType.Int32);
					oParam2.Value = score;
					MySqlParameter oParam3 = cmd.Parameters.Add("?id", MySqlDbType.Int32);
					oParam3.Value = id;
					cmd.ExecuteNonQuery();
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}

	}


	public void insertData(string name,int score)
	{
		try
		{
			string query = "";
			query = "INSERT INTO score (name, score) VALUES (?name, ?score)";
			if (con.State.ToString() != "Open")
				con.Open();

			using (con)
			{
				using (cmd = new MySqlCommand(query, con))
				{
					MySqlParameter oParam1 = cmd.Parameters.Add("?name", MySqlDbType.VarChar);
					oParam1.Value = name;
					MySqlParameter oParam2 = cmd.Parameters.Add("?score", MySqlDbType.Int32);
					oParam2.Value = score;
					cmd.ExecuteNonQuery();
				}
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
		}
	}



	void updateButtonPressed(int id)
	{
		Debug.Log (id);
		idtoupdate = id;
		SceneManager.LoadScene ("update");
	}

	void deleteButtonPressed(int id)
	{
		Debug.Log (id);
	}

	void addButtonPressed()
	{
		SceneManager.LoadScene ("add");
	}


	// Use this for initialization
	void Start () {



		if (SceneManager.GetActiveScene ().name == "output") {
			Button add = GameObject.Find ("addRecordButton").GetComponent<Button> ();
			add.onClick.AddListener (addButtonPressed);
			GetData ();

		}

	}

	// Update is called once per frame
	void Update () {

	}
}
